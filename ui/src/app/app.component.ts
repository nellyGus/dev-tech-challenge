import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from './app.service';
import {Argonaute} from './argonaute';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  argonauteList$: Argonaute[];

  itemForm: FormGroup = this.fb.group({
    id: [],
    nom: ['', [Validators.required]],
  });

  constructor(private fb: FormBuilder,
              private service: AppService) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  private getAll() {
    this.service.getAll()
      .subscribe(resp => {
        this.argonauteList$ = resp;
      });
  }

  onSave() {
    this.service.create(this.itemForm.value)
      .subscribe(resp => {
        this.itemForm.reset();
        this.ngOnInit();
      }, resp => {
        console.log(resp.error.detail);
      });
  }

  get fc() {
    return this.itemForm.controls;
  }
}
