import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Argonaute} from "./argonaute";
import {Observable} from "rxjs";

const apiPath = environment.baseUrl + '/api/argonautes'

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Argonaute[]> {
    return this.http.get<Argonaute[]>(apiPath);
  }

  create(argonaute: Argonaute): Observable<Argonaute> {
    return this.http.post<Argonaute>(apiPath, argonaute);
  }
}
